/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         Nokia Ringtone player
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          26.03.20018
 *
 * $Id: soundPlayer.h 116 2018-05-11 14:30:59Z zajost $
 *
 *--------------------------------------------------------------------
 */

#ifndef SOURCES_SOUNDPLAYER_H_
#define SOURCES_SOUNDPLAYER_H_


bool soundIsPlaying(void);
void soundTooglePlayPause(void);
void soundStart(void);
void soundStop(void);
void soundLoad(const char *soundFile);
void soundPlay(const char *soundFile);
void soundPlayerInit(void);

#endif /* SOURCES_SOUNDPLAYER_H_ */
