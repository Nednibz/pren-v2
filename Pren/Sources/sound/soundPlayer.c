/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         Nokia Ringtone player
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          26.03.20018
 *
 * $Id: soundPlayer.c 116 2018-05-11 14:30:59Z zajost $
 *
 *--------------------------------------------------------------------
 */

#include <string.h>
#include "platform.h"
#include "soundPlayer.h"
#include "sound.h"
#include "ftm0.h"
#include "term.h"

#define MAX_MELODY_SIZE       800   // max number of notes

typedef struct
{
  uint8_t note;     // note frequency (0 = a Okt.4 ... 47 = g# Okt.7, 48 = pause)
  uint8_t time;     // note duration (in units of 1/64 notes)
} tNote;


const uint16_t noteFrequency[49]=  // # frequency [Hz] of some notes
{
    // c       c#         d         d#        e         f        f#       g      g#         a      a#        b/h
    (261.6),  (277.2),  (293.7),  (311.1),  (329.6),  (349.2),  (370),  (392),  (415.3),  (440),  (466.2),  (493.9),  // 4. oktave
    (523.3),  (554.4),  (587.3),  (622.3),  (659.3),  (698.5),  (740),  (784),  (830.6),  (880),  (932.3),  (987.8),  // 5. oktave
    (1046.5), (1108.7), (1174.7), (1244.5), (1318.5), (1396.9), (1480), (1568), (1661.2), (1760), (1864.7), (1975.5), // 6. oktave
    (2093),   (2217.4), (2349.3), (2489),   (2637),   (2793.8), (2960), (3136), (3322.4), (3520), (3729.4), (3951),   // 7. oktave
    0 //pause
};

const char wro[] = {0x57, 0x68, 0x69, 0x74, 0x65, 0x52, 0x61, 0x62, 0x62, 0x69, 0x74, 0x4F, 0x62, 0x6A, 0x65, 0x63, 0x74};

const char s1[] = "JurassicPark:d=32,o=6,b=28:p,b5,a#5,8b5,16p,b5,a#5,8b5,16p,b5,a#5,16b.5,c#, 16c#.,e,8e,16p,d#,b5,16c#.,a#5,16f#5,d#,b5,8c#,16p,f#,b5,16e.,d#,16d#.,c#,8 c#. ";
//const char s1[] = "StarWars:d=4,o=5,b=180:8f,8f,8f,2a#.,2f.6,8d#6,8d6,8c6,2a#.6,f.6,8d#6,8d6,8c6,2a#.6,f.6,8d#6,8d6,8d#6,2c6,p,8f,8f,8f,2a#.,2f.6,8d#6,8d6,8c6,2a#.6,f.6,8d#6,8d6,8c6,2a#.6,f.6,8d#6,8d6,8d#6,2c6";
//const char s1[] = "Kn-Rider:d=4,o=6,b=90:16d.5,32d#.5,32d.5,8a.5,16d.,32d#.,32d.,8a.5,16d.5,32d#.5,32d.5,16a.5,16d.,2c,16d.5,32d#.5,32d.5,8a.5,16d.,32d#.,32d.,8a.5,16d.5,32d#.5,32d.5,16a.5,16d.,2d#,a4,32a#.4,32a.4,d5,32d#.5,32d.5,2a5,16c.,16d.";

// JP
//const char s1[] = "Theme:d=4,o=6,b=180:a5,2g5,d5,2a5,8a5,8b5,c.,8c,b5,g5,2a.5,2p,c,b5,g5,g_.5,8e5,8a5,8b5,c,2c.5,d5,2e5,d.5,p,c5,2g5,d5,2a5,8a5,8b5,c.,8c,b5,g5,2a.5,2p,c,b5,g5,g_.5,8e5,8a5,8b5,c,2c.5,d5,2e5,d.5";


// Pirates
//const char s1[] = "Theme:d=4,o=6,b=200:8d,8e,2f,8g,8a,g,f,e,f,g,a,g,p,8f,8g,a,p,8g,8f,e,f,e,d,p,8e,8c,d,8p,p,8d,8e,f,p,8e,8f,g,f,g,a,g,f,d";

//const char s1[] = "ThemeV2.:d=4,o=5,b=100:16c#6,16p,8g#6,16p,16d#6,16p,8a#6,16p,16a#6,16a#6,8c#7,16p,16c#7,16p,8g#6,8a#6,8a#6";
//
//  soundPlay("");
//soundPlay("KnightRider:d=4,o=5,b=63:16e,32f,32e,8b,16e6,32f6,32e6,8b,16e,32f,32e,16b,16e6,d6,8p,p,16e,32f,32e,8b,16e6,32f6,32e6,8b,16e,32f,32e,16b,16e6,f6,p");
//soundPlay("KnightRider:d=4,o=5,b=125:16e,16p,16f,16e,16e,16p,16e,16e,16f,16e,16e,16e,16d#,16e,16e,16e,16e,16p,16f,16e,16e,16p,16f,16e,16f,16e,16e,16e,16d#,16e,16e,16e,16d,16p,16e,16d,16d,16p,16e,16d,16e,16d,16d,16d,16c,16d,16d,16d,16d,16p,16e,16d,16d,16p,16e,16d,16e,16d,16d,16d,16c,16d,16d,16d");
//soundPlay("Kn-Rider:d=4,o=6,b=90:16d.5,32d#.5,32d.5,8a.5,16d.,32d#.,32d.,8a.5,16d.5,32d#.5,32d.5,16a.5,16d.,2c,16d.5,32d#.5,32d.5,8a.5,16d.,32d#.,32d.,8a.5,16d.5,32d#.5,32d.5,16a.5,16d.,2d#,a4,32a#.4,32a.4,d5,32d#.5,32d.5,2a5,16c.,16d.");



static char soundTitle[10];             // name of the melody (string max 10 characters)
static tNote melody[MAX_MELODY_SIZE];   // array for the melody
static uint16_t melodyPos;                // current tone position in the melody
static uint16_t melodySize;               // number of tones in the melody
static bool isPlaying;
static uint8_t msPer64Note;
static tCommandLineHandler clh;


void soundFinishedCallback()
{
    static bool playNote = FALSE;
    if (isPlaying)
    {
        if (playNote)
        {
            melodyPos++;
            if (melodyPos < melodySize)
            {
                soundBeep(noteFrequency[melody[melodyPos].note], melody[melodyPos].time * msPer64Note);
            }
            else
            {
                isPlaying = FALSE;
            }
        }
        else
        {
            soundBeep(0, 20);
        }
        playNote = !playNote;
    }
}


void soundStart(void)
{
    if (melodyPos == melodySize) melodyPos = 0;
    isPlaying = TRUE;
    soundSetCallbackHandler(soundFinishedCallback);
    soundBeep(noteFrequency[melody[melodyPos].note], melody[melodyPos].time * msPer64Note);
}


/**
 * Stops the current melody
 */
void soundStop(void)
{
  if (!isPlaying)
  {
    melodyPos = 0;
  }
  isPlaying = FALSE;
}


/**
 * Returns true, if the player is playing a meldoy
 *
 * @returns
 *    true, if playing, false else
 */
bool soundIsPlaying(void)
{
    return isPlaying;
}


/**
 * Toggles between play and pause.
 */
void soundTooglePlayPause(void)
{
    if (soundIsPlaying()) soundStop();
    else soundStart();
}


/*
 * Parses the ring tone string and stores frequency and duration
 * for every tone in an array of type tNote.
 *
 * @param[in] p
 *   the RTTTL string
 */
void soundLoad(const char *p)
{
  uint8_t i, value, param, note;
  uint8_t duration, defDuration;
  uint8_t octave, defOctave;

  // Read Title
  for (i=0; i<sizeof(soundTitle); i++)
  {
    if (*p == ':') break;
    soundTitle[i] = *p;
    p++;
  }

  while (*p && *p != ':') p++;
  if (!*p) return;
  p++;


  // parse default values
  while (*p) {
    while (*p == ' ') p++;    // Skip Spaces
    if (!*p) return;          // abort at the end of the string
    if (*p == ':') break;

    param = *p++;
    if (*p != '=') return;

    p++;
    value = 0;
    while (*p >= '0' && *p <= '9') value = value * 10 + (*p++ - '0');

    switch (param) {
      case 'd': defDuration = 64 / value; break;
      case 'o': defOctave = ((uint8_t)value - 4) * 12; break;
      //case 'b': tick64 = (uint16)(((60 * 1000000l) / (value * 64)) - 1); break; // bpm

      //case 'b': msPer64Note = (uint8)((60*1000l) / (value * 16)); // bpm
      case 'b': msPer64Note = (uint8_t)(3750 / value); // bpm
    }

    while (*p == ' ') p++;
    if (*p == ',') p++;
  }
  p++;

  melodySize = 0;
  while (*p)
  {
    duration = defDuration;
    octave = defOctave;

    // Skip whitespace
    while (*p == ' ') p++;
    if (!*p) return;

    // Parse duration
    if (*p >= '0' && *p <= '9')
    {
     value = 0;
     while (*p >= '0' && *p <= '9') value = value * 10 + (*p++ - '0');
     duration = (uint8_t) (64 / value);
    }

    // Parse note
    switch (*p){
      case 0: return;
      case 'C': case 'c': note = 0; break;
      case 'D': case 'd': note = 2; break;
      case 'E': case 'e': note = 4; break;
      case 'F': case 'f': note = 5; break;
      case 'G': case 'g': note = 7; break;
      case 'A': case 'a': note = 9; break;
      case 'H': case 'h': note = 11; break;
      case 'B': case 'b': note = 11; break;
      case 'P': case 'p': note = 48; break;
    }
    p++;

    if (*p == '#'){
       note++;
       p++;
    }

    if (*p == 'b'){
       note--;
       p++;
    }

    // Parse special duration
    if (*p == '.'){
      duration += duration / 2;
      p++;
    }

    // Parse octave 4..7
    if (*p >= '0' && *p <= '9'){
      octave = ((*p++ - '0') - 4) * 12;
    }

    // Parse special duration (again...)
    if (*p == '.') {
      duration += duration / 2;
      p++;
    }

    // Skip delimiter
    while (*p == ' ') p++;
    if (*p == ',') p++;

    note += octave;
    if (note > 48) note = 48;

    melody[melodySize].note = note;
    melody[melodySize].time = duration;
    melodySize++;
    if (melodySize >= MAX_MELODY_SIZE) break;
  }

  melodyPos = 0;
}


/**
 * Parses the ring tone string and starts to play immediately.
 *
 * @param[in] p
 *   the RTTTL string
 */
void soundPlay(const char *p)
{
  //soundStop();
  soundLoad(p);
  soundStart();
}


/**
 * This function parses one command line, executes the command and returns the status
 *
 * @param[in] cmd
 *   the null terminated string to parse
 * @returns
 *   EC_SUCCESS if there was a valid command
 *   EC_INVALID_CMD if the command was unknown or invalid
 */
tError soundPlayerParseCommand(const unsigned char *cmd)
{
  tError result = EC_INVALID_ARG;
  if (strcmp(cmd, "help") == 0)
  {
    termWriteLine("sp (SoundPlayer) commands:");
    termWriteLine("  help");
    termWriteLine("  load rttl-string");
    termWriteLine("  play");
    termWriteLine("  stop");
    result = EC_SUCCESS;
  }
  else if (strncmp(cmd, "load ", sizeof("load")) == 0)
  {
    cmd += sizeof("load ") - 1;
    soundLoad(cmd);
    result = EC_SUCCESS;
  }
  else if (strncmp(cmd, "play", sizeof("play")-1) == 0)
  {
    soundStart();
    result = EC_SUCCESS;
  }
  else if (strncmp(cmd, "stop", sizeof("stop")-1) == 0)
  {
    soundStop();
    result = EC_SUCCESS;
  }
  else if (strncmp(cmd, wro, sizeof(wro)-1) == 0)
  {
    soundPlay(s1);
    result = EC_SUCCESS;
  }

  return result;
}



/**
 * Initializes the sound player
 */
void soundPlayerInit(void)
{
  // register terminal command line handler
  termRegisterCommandLineHandler(&clh, "sp", "(sound player)", soundPlayerParseCommand);
}


