/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         Buzzer driver to generate different frequencies
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          26.03.20018
 *
 * $Id: sound.h 116 2018-05-11 14:30:59Z zajost $
 *
 *--------------------------------------------------------------------
 */

#ifndef SOURCES_SOUND_H_
#define SOURCES_SOUND_H_


typedef void (*soundFinishedHandler)(void);

void soundSetCallbackHandler(soundFinishedHandler handler);
void soundBeep(uint16_t frequency, uint16_t timeMS);
void soundInit(void);


#endif /* SOURCES_SOUND_H_ */
