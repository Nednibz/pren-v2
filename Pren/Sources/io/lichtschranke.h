/*
 * lichtschranke.h
 *
 *  Created on: 14.03.2019
 *      Author: zbind
 */

#ifndef SOURCES_IO_LICHTSCHRANKE_H_
#define SOURCES_IO_LICHTSCHRANKE_H_

#include"stdbool.h"
#include "Errors.h"


#define LS1_Mask 0b10
#define LS2_Mask 0b100

bool LS1(void);
bool LS2(void);
general_errors_t LSInit(void);

#endif /* SOURCES_IO_LICHTSCHRANKE_H_ */
