/*
 * lichtschranke.c
 *
 *  Created on: 14.03.2019
 *      Author: zbind
 */
#include "platform.h"
#include "Errors.h"
#include"stdbool.h"
#include"lichtschranke.h"

general_errors_t LSInit(void){

PORTD_PCR1 = PORT_PCR_MUX(1) | PORT_PCR_PE(1) | PORT_PCR_PS(1); //Lichtschranke 1
PORTD_PCR2 = PORT_PCR_MUX(1) | PORT_PCR_PE(1) | PORT_PCR_PS(1); //Lichtschranke 2

return NO_ERROR;
}
bool LS1(void){
	uint32_t z = 0b10 & ~GPIOD_PDIR;
	return 0b10 & ~GPIOD_PDIR;
}
bool LS2(void){
	uint32_t z = 0b100 & ~GPIOD_PDIR;
	return 0b100 & ~GPIOD_PDIR;
}
