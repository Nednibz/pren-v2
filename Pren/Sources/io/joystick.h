/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         joystick driver of the MC-Car
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          27.04.20018
 *
 * $Id: joystick.h 116 2018-05-11 14:30:59Z zajost $
 *
 *--------------------------------------------------------------------
 */
#ifndef SOURCES_IO_JOYSTICK_H_
#define SOURCES_IO_JOYSTICK_H_


typedef enum
{
  jsRight   = (1<<0), // PTB0
  jsDown    = (1<<1), // PTB1
  jsUp      = (1<<2), // PTB2
  jsCenter  = (1<<3), // PTB3
  jsLeft    = (1<<9)  // PTB9
} tJoystick;


tJoystick joystick(void);
void joystickInit(void);


#endif /* SOURCES_IO_JOYSTICK_H_ */
