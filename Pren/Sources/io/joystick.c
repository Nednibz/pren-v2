/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         joystick driver of the MC-Car
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          27.04.20018
 *
 * $Id: joystick.c 116 2018-05-11 14:30:59Z zajost $
 *
 *--------------------------------------------------------------------
 */

#include "platform.h"
#include "joystick.h"

tJoystick joystick(void)
{
  return (0b0000001000001111 & ~GPIOB_PDIR);
}


/**
 * Up:     PTB2
 * Dn:     PTB1
 * Right:  PTB0
 * Left:   PTB9
 * Center: PTB3
 */
void joystickInit(void)
{
  PORTB_PCR0 = PORT_PCR_MUX(1) | PORT_PCR_PE(1) | PORT_PCR_PS(1);
  PORTB_PCR1 = PORT_PCR_MUX(1) | PORT_PCR_PE(1) | PORT_PCR_PS(1);
  PORTB_PCR2 = PORT_PCR_MUX(1) | PORT_PCR_PE(1) | PORT_PCR_PS(1);
  PORTB_PCR3 = PORT_PCR_MUX(1) | PORT_PCR_PE(1) | PORT_PCR_PS(1);
  PORTB_PCR9 = PORT_PCR_MUX(1) | PORT_PCR_PE(1) | PORT_PCR_PS(1);
}
