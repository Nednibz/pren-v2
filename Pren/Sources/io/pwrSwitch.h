/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         Power-Switch helper functions
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          17.04.20018
 *
 * $Id: pwrSwitch.h 116 2018-05-11 14:30:59Z zajost $
 *
 *--------------------------------------------------------------------
 */

#ifndef SOURCES_UTILS_PWRSWITCH_H_
#define SOURCES_UTILS_PWRSWITCH_H_



/**
 * Initializes the Power Switch
 * Mux: GPIO
 * PE: Pull-Enable
 * PS: Pull-Up
 */
#define pwrSwitchInit()     (PORTA_PCR4 = PORT_PCR_MUX(Z) | PORT_PCR_PE(1) | PORT_PCR_PS(1))


/**
 * Returns the state of the PowerSwitch
 * 0: Power Switch is in the default position
 * 1: Power Switch is enabled
 */
#define pwrSwitchEnabled()  (!(GPIOA_PDIR & (1<<4)))



#endif /* SOURCES_UTILS_PWRSWITCH_H_ */
