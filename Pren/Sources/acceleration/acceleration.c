/*
 * acceleration.c
 *
 *  Created on: 02.11.2018
 *      Author: zbind
 */

#include "platform.h"
#include "acceleration.h"
#include "i2c.h"
#include "util.h"
#include"Errors.h"
#include "stdbool.h"

general_errors_t AccInit(){

	general_errors_t result = NO_ERROR;
	uint8_t databyte = 0;
	uint8_t write_reg = 0;
	uint8_t base_adr = ACC_ADDRESS;
	tError err = EC_SUCCESS;

	//tError test =  i2cTest(base_adr);
	//switch(test){}TODO error handling
	// Configure the output resolution of the Accelerometer
	write_reg = CTRL1_XL;
	databyte = ODR_MASK;
	err += i2cWriteCmdData(base_adr, write_reg, &databyte, sizeof(databyte));
	utilWaitUs(20);
	// Configure the output resolution of the Gyro
	write_reg = TRL10_C;
	databyte = TRL10_C_MASK;
	err += i2cWriteCmdData(base_adr, write_reg, &databyte, sizeof(databyte));
	utilWaitUs(20);
	// enable the output of the Accelerometer
	write_reg = TRL9_XL;
	databyte = TRL9_XL_MASK;
	err += i2cWriteCmdData(base_adr, write_reg, &databyte, sizeof(databyte));
	utilWaitUs(20);
		//Todo enable Gyro Outputs

	// Configure the FIFO
	write_reg = FO_CTRL5;
	databyte = FO_CTRL5_MASK;
	err += i2cWriteCmdData(base_adr, write_reg, &databyte, sizeof(databyte));
	utilWaitUs(20);
	write_reg = TRL3_C;
	databyte = TRL3_C_MASK;
	err += i2cWriteCmdData(base_adr, write_reg, &databyte, sizeof(databyte));

	utilWaitUs(20);
	  //Todo: errorhandling

	  return result;
}

acceleration getacceleration(void){
	uint8_t value = 0;
	uint16_t databyte = 0;
	uint16_t write_reg = 0;
	uint16_t base_adr = ACC_ADDRESS;
	acceleration response_vector = {0,0,0,false};
	tError err;
	if(i2cTest(base_adr) == EC_SUCCESS){
		response_vector.validity = true;
	}


	write_reg = OUTX_H_XL;
	i2cReadCmdData(base_adr, write_reg, &value, 1);
	response_vector.x =  (value<<8);
	write_reg = OUTX_L_XL;
	i2cReadCmdData(base_adr, write_reg, &value, 1);
	response_vector.x |= value;

	write_reg = OUTY_H_XL;
	i2cReadCmdData(base_adr, write_reg, &value, 1);
	response_vector.y =  (value<<8);
	write_reg = OUTY_L_XL;
	i2cReadCmdData(base_adr, write_reg, &value, 1);
	response_vector.y |= value;

	write_reg = OUTZ_H_XL;
	i2cReadCmdData(base_adr, write_reg, &value, 1);
	response_vector.z =  (value<<8);
	write_reg = OUTZ_L_XL;
	i2cReadCmdData(base_adr, write_reg, &value, 1);
	response_vector.z |= value;

	response_vector.x *= LA_So;
	response_vector.y *= LA_So;
	response_vector.z *= LA_So;

	 return response_vector; // in mili g
}

acceleration getrotation(void){
	uint8_t databyte = 0;
	uint8_t write_reg = 0;
	uint8_t base_adr = ACC_ADDRESS;
	int8_t x, y, z;
	acceleration response_vector;

/*
	write_reg = OUTX_H_XL;
	i2cReadCmdData(base_adr, write_reg, &response_vector.x, 1);

    write_reg = OUTY_H_XL;
    i2cReadCmdData(base_adr, write_reg, &response_vector.y, 1);

	 write_reg = OUTZ_H_XL;
	 i2cReadCmdData(base_adr, write_reg, &response_vector.z, 1);
*/
	 return response_vector;

}



