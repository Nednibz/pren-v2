/*
 * acceleration.h
 *
 *  Created on: 02.11.2018
 *      Author: zbind
 */

#ifndef SOURCES_ACCELERATION_ACCELERATION_H_
#define SOURCES_ACCELERATION_ACCELERATION_H_
#include "i2c.h"
#include "Errors.h"
#include"stdbool.h"

typedef struct acceleration {
	   int16_t x;
	   int16_t y;
	   int16_t z;
	   bool validity;
	}acceleration;

#define LA_FS	 				2						// Linear acceleration measurement range +- 2g
#define G_FS 	  				250						// Angular ratemeasurement range +- 250 dps
#define LA_So			  		0.061					// Linear acceleration sensitivity factor +- 2 mg
#define LA_OS				   	2000					// offset = 2000 mg
#define G_So  					4.375					// Angular rate sensitivity

#define G_ODR  					52						// Angular rate output data rate
#define FUNC_CFG_ACCESS  		0x00					// Embedded functions configuration register
#define SENSOR_SYNC_TIME_FRAME  0x04					// Sensor sync configuration register
	//********************Accelerometer and gyroscope control registers****************

#define ACC_ADDRESS 			0x6B

	//********************Accelerometer and gyroscope control registers****************
#define CTRL1_XL  				0x10
#define CTRL2_G 		 		0x11
#define CTRL3_C  				0x12
#define CTRL4_C			  		0x13
#define CTRL5_C		 	 		0x14
#define CTRL6_C  				0x15
#define CTRL7_G  				0x16
#define CTRL8_XL 				0x17
#define CTRL9_XL 				0x18
#define CTRL10_C 				0x09
#define FO_CTRL5  				0x0A
#define TRL9_XL       		 	0x18
#define TRL10_C      		  	0x19
#define TRL3_C	      			0x12

//***********************general control registers****************
#define MASTER_CONFIG			0x1A
#define STATUS_REG 				0x1E
	//********************Gyroscope output register****************
#define OUTX_L_G 				0x22
#define OUTX_H_G 				0x23
#define OUTY_L_G 				0x24
#define OUTY_H_G 				0x25
#define OUTZ_L_G 				0x26
#define OUTZ_H_G 				0x27
	//********************Accelerometer output registe****************
#define OUTX_L_XL 				0x28
#define OUTX_H_XL 				0x29
#define OUTY_L_XL 				0x2A
#define OUTY_H_XL 				0x2B
#define OUTZ_L_XL 				0x2C
#define OUTZ_H_XL 				0x2D

#define ODR_MASK 				0b01100000				//Output data rate
#define FS_XL_MASK 				0b0						//Output data rate
#define BW_XL_MASK 				0b0						//Output data rate
#define FO_CTRL5_MASK 			0b00001110				//FIFO Mode
#define TRL9_XL_MASK			0b00111000				//enable acceleration Outputs
#define TRL10_C_MASK			0b00111000				//enable Gyro Outputs
#define TRL3_C_MASK				0b000



general_errors_t AccInit();
acceleration getacceleration(void);
acceleration getrotation(void);


#endif /* SOURCES_ACCELERATION_ACCELERATION_H_ */
