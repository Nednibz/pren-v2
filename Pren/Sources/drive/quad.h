/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         quadrature decoder
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          03.04.20018
 *
 * $Id: quad.h 116 2018-05-11 14:30:59Z zajost $
 *
 *--------------------------------------------------------------------
 */
#ifndef SOURCES_DRIVE_QUAD_H_
#define SOURCES_DRIVE_QUAD_H_

	/**
	 * QDCTRL register:
	 * |		31-8		 |
	 * | 		read only	 |
	 * ------------------------
	 * |		7		|		6		|		5		|		4		|		3		|		2		|		1		|		0		|
	 * |	PHAFLTREN	|	PHBLENTREN	|		PHAPOL	|	PHBPOL		|	QUADMODE	|	QUADDIR		|	TOFDIR		|	QUADEN		|
	 */
#define FILTR_A			0<<7	//filter port A enabled or disabled
#define FILTR_B			0<<6	//filter port B enabled or disabled
#define POL_A			0<<5	//input inverse or normal
#define POL_B			0<<4	//input inverse or normal
#define QUADMODE		0<<3	//0 Phase A and phase B encoding mode. 1 Count and direction encoding mode.
#define QUADDIR			0<<2	//0 Counting direction is decreasing (FTM counter decrement). 1 Counting direction is increasing (FTM counter increment)
#define TOFDIR			0<<1	//Indicates if the TOF bit was set on the top or the bottom of counting.
#define QUADEN			1<<0	//Quadrature encoder enable
#define QDCRTL_MASK			FILTR_A | FILTR_B | POL_A | POL_B | QUADMODE | QUADEN




//Encoder A vorne	PTB18
//Encoder B vorne	PTB19
//Encoder A hinten PTA12
//Encoder B hinten PTA13


int16_t quadGetSpeedBack(void);
int16_t quadGetSpeedFront(void);
int16_t quadGetDistanceFront(void);
int16_t quadGetDistanceBack(void);
void quadInit(void);

#endif /* SOURCES_DRIVE_QUAD_H_ */
