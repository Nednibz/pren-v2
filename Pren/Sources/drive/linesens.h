/*
 * linesens.h
 *
 *  Created on: 07.05.2018
 *      Author: zajost
 */

#ifndef SOURCES_DRIVE_LINESENS_H_
#define SOURCES_DRIVE_LINESENS_H_



void lsCalibWhite(void);
void lsCalibBlack(void);
uint8_t lsGetSensData(void);
int8_t lsGetDir(void);
void lsInit(void);



#endif /* SOURCES_DRIVE_LINESENS_H_ */
