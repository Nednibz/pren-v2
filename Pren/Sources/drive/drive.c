/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         Drive with PID
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          04.04.20018
 *
 * $Id: drive.c 116 2018-05-11 14:30:59Z zajost $
 *
 *--------------------------------------------------------------------
 */

#include "platform.h"
#include "drive.h"
#include "quad.h"
#include "motor.h"

static int16_t setValueBack;
static int16_t setValueFront;
static uint8_t kpL, kiL, kdL;
static uint8_t kpR, kiR, kdR;
static int16_t integL, devL, devOldL;
static int16_t integR, devR, devOldR;
static int32_t valL, valR;



int16_t sl[256];
int16_t sr[256];
uint8_t sli, sri;



/**
 * Sets the speed
 * @param[in] speedL
 *   the speed of the left wheel in mm/sec
 * @param[in] speedR
 *   the speed of the right wheel in mm/sec
 */
void driveStep(int8_t step)
{
	int16_t distance =  quadGetDistanceFront();
	while(quadGetDistanceFront() < distance + step){
		driveSetSpeed(STEP_SPEED);
	}
	while(quadGetDistanceFront() > distance + step){
			driveSetSpeed(-STEP_SPEED);
		}
	driveSetSpeed(0);
  // TODO write function for stepping
}

/**
 * Sets the speed
 * @param[in] speedL
 *   the speed of the left wheel in mm/sec
 * @param[in] speedR
 *   the speed of the right wheel in mm/sec
 */
void driveSetSpeed(int16_t speed)
{
  setValueBack = speed;
  setValueFront = speed;
}


/**
 * This function sets the control parameters
 * @param[in] pKpL Kp left 0..255
 * @param[in] pKpR Kp right 0..255
 * @param[in] pKiL Ki left 0..255
 * @param[in] pKiR Ki right 0..255
 */
void driveSetParameters(uint8_t pKpL, uint8_t pKpR, uint8_t pKiL, uint8_t pKiR)
{
  kpL = pKpL;
  kpR = pKpR;
  kiL = pKiL;
  kiR = pKiR;
}

#define a 2000
/**
 * This function contains the PID closed loop controller
 */
void driveToWork(void)
{
  static int16_t speedLeft = 0;
  static int16_t speedRight = 0;
  static int16_t setValueL = 0;
  static int16_t setValueR = 0;

  motorSetPwmFront(0);
  motorSetPwmBack(0);

  speedLeft = ((speedLeft * 1) + quadGetSpeedBack())/2;
  speedRight = ((speedRight * 1) + quadGetSpeedFront())/2;

  if (setValueFront > setValueR) {                                        // accelerate right wheel
    setValueR += (a/40);
    if (setValueFront < setValueR) setValueR = setValueFront;
  }
  if (setValueFront < setValueR) {                                        // decelerate right wheel
    setValueR -= (a/40);
    if (setValueFront > setValueR) setValueR = setValueFront;
  }

  if (setValueBack > setValueL) {                                         // decelerate left wheel
    setValueL += (a/40);
    if (setValueBack < setValueL) setValueL = setValueBack;
  }
  if (setValueBack < setValueL) {                                         // decelerate left wheel
    setValueL -= (a/40);
    if (setValueBack > setValueL) setValueL = setValueBack;
  }

  if (setValueL)
  {
    devL = (setValueL - speedLeft);       // calc deviation: max devL = +2000 - -2000 = 4000
    valL = (kpL * devL);                  // P-Part: max (kpL * devL) = 1024000
    if (kiL) integL += devL;              // I-Part with anti-windup
    valL += (kiL * integL);
    valL += (kdL*(setValueL-devOldL));    // D-Part
    devOldL = setValueL;
    valL /= 1000;                         // scaling

    // pre control
    valL += (67 * setValueL) / 1000 + (setValueL > 0 ? 10 : -10);

    if (valL > MOTOR_MAX_VALUE) {
      valL = MOTOR_MAX_VALUE;
      integL -= devL;
    }
    else if (valL < -MOTOR_MAX_VALUE) {
      valL = -MOTOR_MAX_VALUE;
      integL += devL;
    }
  }
  else {
    valL = integL = devOldL = 0;
  }

  if (setValueR)
  {
    devR = (setValueR - speedRight);      // calc deviation
    valR = (kpR * devR);                  // P-Part: (max kpX = 2000 * 255 = 510'000)
    if (kiR) integR += devR;              // I-Part: with anti-windup below
    valR += (kiR * integR);
    valR += (kdR*(setValueR-devOldR));    // D-Part
    devOldR = setValueR;
    valR /= 1000;                         // scaling

    // pre control
    // y=m*x+n => preControl = setValue*m + n | m=0.055, n=7
    valR += (67 * setValueR) / 1000 + (setValueR > 0 ? 10 : -10);

    if (valR > MOTOR_MAX_VALUE) {
      valR = MOTOR_MAX_VALUE;
      integR -= devR;                     // anti wind-up
    }
    else if (valR < -MOTOR_MAX_VALUE) {
      valR = -MOTOR_MAX_VALUE;
      integR += devR;                     // anti wind-up
    }
  }
  else {
    valR = integR = devOldR = 0;
  }

  sl[sli] = (uint8_t)valL;     // 100=18    500=45     11.25   0.0675
  sr[sli++] = (uint8_t)valR;   // 100=16    500=42    9.5 0.065

  motorSetPwmBack(((int8_t)valL+0));
  motorSetPwmFront((int8_t)valR+0);
}



void driveInit(void)
{
  kpL = kpR = 70;//80;
  kiL = kiR = 20;//30;
  kdL = kdR = 0;
  setValueBack = setValueFront = 00;  //30... 7sec 30m = 4cm/sec
}
