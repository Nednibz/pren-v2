/*
 * linesens.c
 *
 *  Created on: 07.05.2018
 *      Author: zajost
 */
#include "platform.h"
#include "linesens.h"
#include "eeprom.h"
#if !SOLUTION
/*
 * 	IR Diode D11 	PTC6
	IR Diode D12	PTC7
	IR Diode D13	PTC15
	IR Diode D14	PTC17
	IR Diode D15	PTC18

	Bezeichnung 			GPIO-Pin 	FTM/Channel		FTM 	Pin Mux
	Phototransistor Q4 		PTC4	 	FTM0			CH3	 		4
	Phototransistor Q5		PTC5		FTM0			CH2			7
	Phototransistor Q6		PTD6		FTM0			CH6			4
	Phototransistor Q7		PTD7		FTM0			CH7			4
 * */


#define IR_L_ON()               (GPIOC_PCOR = 1<<6)   // clear output enables the IR diode (led is low active)
#define IR_L_OFF()              (GPIOC_PSOR = 1<<6)   // set output disables the IR diode.
#define IR_ML_ON()              (GPIOC_PCOR = 1<<7)
#define IR_ML_OFF()				(GPIOC_PSOR = 1<<7)
#define IR_M_ON()				(GPIOC_PCOR = 1<<15)
#define IR_M_OFF()				(GPIOC_PSOR = 1<<15)
#define IR_MR_ON()				(GPIOC_PCOR = 1<<17)
#define IR_MR_OFF()				(GPIOC_PSOR = 1<<17)
#define IR_R_ON()				(GPIOC_PCOR = 1<<18)
#define IR_R_OFF()				(GPIOC_PSOR = 1<<18)
#define IR_OFF()                (GPIOC_PSOR = (1<<6) | (1<<7) | (1<<15) | (1<<17) | (1<<18))


#define PT_L_OUT_HIGH()         (PORTC_PCR4 = PORT_PCR_MUX(1))
#define PT_L_INPUT_CAPTURE()    (PORTC_PCR4 = PORT_PCR_MUX(4))
#define PT_ML_OUT_HIGH()		(PORTC_PCR5 = PORT_PCR_MUX(1))
#define PT_ML_INPUT_CAPTURE()	(PORTC_PCR5 = PORT_PCR_MUX(7))
#define PT_MR_OUT_HIGH()		(PORTD_PCR6 = PORT_PCR_MUX(1))
#define PT_MR_INPUT_CAPTURE()	(PORTD_PCR6 = PORT_PCR_MUX(4))
#define PT_R_OUT_HIGH()			(PORTD_PCR7 = PORT_PCR_MUX(1))
#define PT_R_INPUT_CAPTURE()	(PORTD_PCR7 = PORT_PCR_MUX(4))


#define PT_L_CH_VALUE           (FTM0_C3V)
#define PT_ML_CH_VALUE			(FTM0_C2V)
#define PT_MR_CH_VALUE			(FTM0_C6V)
#define PT_R_CH_VALUE			(FTM0_C7V)

static uint16_t ticksL;
static uint16_t ticksML;
static uint16_t ticksMR;
static uint16_t ticksR;
static uint16_t ticksStart;
static uint16_t sensData[8];

/**
 * This ISR is called on a falling edge (input capture) of Q4.
 * It measures the charge time of the capacitor, clears the
 * interrupt flag and start the discharge of the capacitor.
 */
void FTM0CH3_IRQHandler(void)
{
  ticksL = FTM0_C3V - ticksStart;       // calc the charge time
  FTM0_C3SC &= ~FTM_CnSC_CHF_MASK;      // clear the channel interrupt flag
  PT_L_OUT_HIGH();                      // discharge the capacitor
}

/**
 * This ISR is called on a falling edge (input capture) of Q5.
 * It measures the charge time of the capacitor, clears the
 * interrupt flag and start the discharge of the capacitor.
 */
void FTM0CH2_IRQHandler(void)
{
  ticksML = FTM0_C2V - ticksStart;
  FTM0_C2SC &= ~FTM_CnSC_CHF_MASK;
  PT_ML_OUT_HIGH();
}

/**
 * This ISR is called on a falling edge (input capture) of Q6.
 * It measures the charge time of the capacitor, clears the
 * interrupt flag and start the discharge of the capacitor.
 */
void FTM0CH6_IRQHandler(void)
{
	ticksMR = FTM0_C6V - ticksStart;
	FTM0_C6SC &= ~FTM_CnSC_CHF_MASK;
	PT_MR_OUT_HIGH();

}

/**
 * This ISR is called on a falling edge (input capture) of Q7.
 * It measures the charge time of the capacitor, clears the
 * interrupt flag and start the discharge of the capacitor.
 */
void FTM0CH7_IRQHandler(void)
{

	ticksR = FTM0_C7V - ticksStart;
	FTM0_C7SC &= ~FTM_CnSC_CHF_MASK;
	PT_R_OUT_HIGH();
}


/**
 * This ISR is called periodically and scans the 8 spots
 *
 *
 *    *             *               *              *              *
 * [IR_L] [PT_L] [IR_ML] [PT_ML] [IR_M] [PT_MR] [IR_MR] [PT_R] [IR_R]
 *      \ /    \ /     \ /     \ /    \ /     \ /     \ /    \ /
 *       0      1       2       3      4       5       6      7
 *
 * state 3      0       0       1      1       2       2      3
 *
 */
void PIT0_IRQHandler(void)
{
  static uint8_t state = 0;
  PIT_TFLG0 |= PIT_TFLG_TIF_MASK;   // clear interrupt flag
  IR_OFF();                         // disable all IR-Diodes

  switch (state)
  {
  case 0:
    sensData[7] = ticksR;           // store the measured time of spot 8
    sensData[0] = ticksL;           // store the measured time of spot 1
    PT_L_INPUT_CAPTURE();           // prepare measure spot 2
    PT_ML_INPUT_CAPTURE();          // prepare measure spot 3
    IR_ML_ON();                     // enable IR diode => start the measure
    state++;
    break;

  case 1:
    sensData[1] = ticksL;           // store the measured time of spot 2
    sensData[2] = ticksML;          // store the measured time of spot 3
    PT_ML_INPUT_CAPTURE();          // prepare measure spot 4
    PT_MR_INPUT_CAPTURE();          // prepare measure spot 5
    IR_M_ON();                      // enable IR diode => start the measure
    state++;
    break;

  case 2:
    sensData[3] = ticksML;          // store the measured time of spot 4
    sensData[4] = ticksMR;          // store the measured time of spot 5
    PT_MR_INPUT_CAPTURE();          // prepare measure spot 6
    PT_R_INPUT_CAPTURE();           // prepare measure spot 7
    IR_MR_ON();                     // enable IR diode => start the measure
    state++;
    break;

  case 3:
    sensData[5] = ticksMR;          // store the measured time of spot 6
    sensData[6] = ticksR;           // store the measured time of spot 7
    PT_R_INPUT_CAPTURE();           // prepare measure spot 8
    PT_L_INPUT_CAPTURE();           // prepare measure spot 1
    IR_R_ON();                      // enable IR diode left and right
    IR_L_ON();                      // => start the measure
    state = 0;
    break;

  default: state = 0; break;
  }

  ticksStart = FTM0_CNT;            // determine the start time of the measure
}


/**
 * White calibration
 * Place the MC-Car on a white area before this function is called
 */
void lsCalibWhite(void)
{
  uint8_t i;
  for (i=0; i<8; i++)
  {
    config.lsCalibDataWhite[i] = sensData[i];
  }
  eepromSave();
}


/**
 * Black calibration
 * Place the MC-Car on a black area before this function is called
 */
void lsCalibBlack(void)
{
  uint8_t i;
  for (i=0; i<8; i++)
  {
    config.lsCalibDataBlack[i] = sensData[i];
  }
  eepromSave();
}


/**
 * Returns the white/black value of the 8 spots
 * LSB: the left sport, MSB: the right spot:
 *
 * @return
 *   the bit coded value of the 8 spots. '1'=black, '0'=white
 */
uint8_t lsGetSensData(void)
{
  uint8_t i;
  int16_t value;
  uint8_t result = 0;

  int16_t data[8];

  uint16_t min = 0xffff;
  uint16_t max = 0;
  uint32_t sum = 0;
  for (i=0; i<8; i++)
  {
    // remove offset
    value = sensData[i] - config.lsCalibDataWhite[i];
    if (value < 0) value = 0;

    // scale
    value = (uint16_t)((value * (uint32_t)1000) / config.lsCalibDataBlack[i]);
    data[i] = value;

    // determine min/max value
    if (value < min) min = value;
    if (value > max) max = value;
    sum += value;
  }
  uint16_t medium = sum / 8;
  int16_t level = medium + ((max - medium) / 4);

  for (i=0; i<8; i++)
  {
    if (data[i] > level) result |= (1<<i);
  }
  return result;
}


/**
 * Returns the deviation of the center: -7..+7
 * +7: the MC-Car is on the right side of the line
 * -7: the MC-Car is on the left side of the line
 *
 * @return
 *   the deviation from the center line
 */
int8_t lsGetDir(void)
{
  uint8_t value;
  int8_t i, v = 0;

  value = lsGetSensData();

  // only white or black spots found
  if (value == 0 || value == 255) return 127;

  // count white spots beginning from the left
  for (i=0; i<8; i++)
  {
    if (value & (1<<i)) break;
    v--;
  }

  // count white spots beginning from the right
  for (i=7; i>=0; i--)
  {
    if (value & (1<<i)) break;
    v++;
  }
  return v;
}


/**
 * Initializes the line sensor driver
 */
void lsInit(void)
{
	PT_L_OUT_HIGH(); // - pin muxing: GPIO
	PT_ML_OUT_HIGH();
	PT_MR_OUT_HIGH();
	PT_R_OUT_HIGH();

	GPIOC_PDDR |= 1<<4 | 1<<5;
	GPIOD_PDDR |= 1<<6 | 1<<7;//- gpio pin direction: output

	GPIOC_PSOR |= 1<<4 | 1<<5;
	GPIOD_PSOR |= 1<<6 | 1<<7; //- gpio pin level: high level (discharge capacitor)



	PORTC_PCR6 = PORT_PCR_MUX(1); // - pin muxing: GPIO
	PORTC_PCR7 = PORT_PCR_MUX(1);
	PORTC_PCR15 = PORT_PCR_MUX(1);
	PORTC_PCR17 = PORT_PCR_MUX(1);
	PORTC_PCR18 = PORT_PCR_MUX(1);

	GPIOC_PDDR |= 1<<6 | 1<<7 | 1<<15 |1<<17 | 1<<18;// - gpio pin direction: output

	IR_OFF();//GPIOC_PSOR = 1<<6 | 1<<7 | 1<<15 |1<<17 | 1<<18;// - gpio pin level: high level (= LED off)



  FTM0_C3SC = FTM_CnSC_ELSx(2) | FTM_CnSC_MSx(0) | FTM_CnSC_CHIE(1);
  FTM0_C2SC = FTM_CnSC_ELSx(2) | FTM_CnSC_MSx(0) | FTM_CnSC_CHIE(1);
  FTM0_C6SC = FTM_CnSC_ELSx(2) | FTM_CnSC_MSx(0) | FTM_CnSC_CHIE(1);
  FTM0_C7SC = FTM_CnSC_ELSx(2) | FTM_CnSC_MSx(0) | FTM_CnSC_CHIE(1);


  // configure Periodic Interrupt Timer (PIT)
  SIM_SCGC6 |= SIM_SCGC6_PIT_MASK;                    // configure clock gating
  PIT_MCR = PIT_MCR_MDIS(0);                          // enable PIT
  PIT_LDVAL0 = 750000;                                // Timer Load Value (60e6MHz / 750000 => 12.5ms)
  PIT_TCTRL0 = PIT_TCTRL_TIE(1) | PIT_TCTRL_TEN(1);   // enable interrupt & PIT channel
  NVIC_SetPriority(PIT0_IRQn, PRIO_PIT0);             // set interrupt priority
  NVIC_EnableIRQ(PIT0_IRQn);                          // enable interrupt
}
#endif
