/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         Drive with PID
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          04.04.20018
 *
 * $Id: drive.h 116 2018-05-11 14:30:59Z zajost $
 *
 *--------------------------------------------------------------------
 */

#ifndef SOURCES_DRIVE_DRIVE_H_
#define SOURCES_DRIVE_DRIVE_H_

#define MAX_SAFE_ACC		500 //TODO define max acc and speed
#define MAX_SPEED			0
#define STEP_SPEED			1 //TODO define stepspeed
#define LOAD_SPEED			5 //TODO define load speed
#define MAX_LOAD_DISTANCE	50 //TODO define max load distance
#define IR_SPEED			20 //TODO define IR speed


void driveSetParameters(uint8_t pKpL, uint8_t pKpR, uint8_t pKiL, uint8_t pKiR);
void driveSetSpeed(int16_t speed);
void driveStep(int8_t step);
void driveToWork(void);
void driveInit(void);


#endif /* SOURCES_DRIVE_DRIVE_H_ */
