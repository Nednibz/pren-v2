/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         motor driver
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          03.04.20018
 *
 * $Id: motor.c 116 2018-05-11 14:30:59Z zajost $
 *
 *--------------------------------------------------------------------
*/

#include "platform.h"
#include "motor.h"
#include "ftm0.h"
#include "term.h"
#include "util.h"
#include <string.h>


#define MOTOR_BACK_BACKWARDS()          GPIOD_PSOR = 1<<3; GPIOD_PCOR = 1<<4
#define MOTOR_BACK_FORWARDS()           GPIOD_PCOR = 1<<4; GPIOD_PSOR = 1<<4
#define MOTOR_BACK_STOP()				GPIOD_PSOR = 1<<3 | 1<<4

#define MOTOR_FRONT_BACKWARDS()         GPIOD_PSOR = 1<<5; GPIOD_PCOR = 1<<6
#define MOTOR_FRONT_FORWARDS()          GPIOD_PCOR = 1<<5; GPIOD_PSOR = 1<<6
#define MOTOR_FRONT_STOP()				GPIOD_PSOR = 1<<5 | 1<<6

static uint16_t valueFront;
static uint16_t valueBack;



/**
 * Increments or decrements the PWM value oft the right wheel
 * @param[in] value
 *   a positive or negative value to add
 */
void motorIncrementPwmFront(int8_t value)
{
  int32_t v = valueFront + value;
  if (v > MOTOR_MAX_VALUE) v = MOTOR_MAX_VALUE;
  if (v < -MOTOR_MAX_VALUE) v = -MOTOR_MAX_VALUE;
  motorSetPwmFront((int8_t)v);
}

/**
 * Increments or decrements the PWM value oft the left wheel
 * @param[in] value
 *   a positive or negative value to add
 */
void motorIncrementPwmBack(int8_t value)
{
  int32_t v = valueBack + value;
  if (v > MOTOR_MAX_VALUE) v = MOTOR_MAX_VALUE;
  if (v < -MOTOR_MAX_VALUE) v = -MOTOR_MAX_VALUE;
  motorSetPwmBack((int8_t)v);
}


/**
 * Sets the PWM value of the right wheel
 *
 * @param[in] value
 *   the value between -MOTOR_MAX_VALUE..0..+MOTOR_MAX_VALUE
 *   A value of '0' stops the wheel.
 *
 *  Initializes the motor driver:
* - IN3 H ; IN4 L --> forward
* - IN3 L ; IN4 H --> backward
* - IN3 =  IN4 	  --> fast stop
* - IN3 != IN4	  --> slow stop
 */
void motorSetPwmFront(int8_t value)
{
  if (value > MOTOR_MAX_VALUE) value = MOTOR_MAX_VALUE;
  if (value < -MOTOR_MAX_VALUE) value = -MOTOR_MAX_VALUE;
  valueFront = value;

  if (value < 0)
  {
    // drive backward
    value = -value;             // value has to be a positive channel value!
    MOTOR_FRONT_BACKWARDS();
  }
  else if (value > 0)
  {
    // drive forward
	  MOTOR_FRONT_FORWARDS();
  }
  else
  {
    // stop
	  MOTOR_FRONT_STOP();
  }
  uint16_t v = (uint16_t)(((FTM0_MOD + 1) * ((uint32_t)value)) / MOTOR_MAX_VALUE);
  FTM0_C1V = v;
}


/**
 * Sets the PWM value of the left wheel
 *
 * @param[in] value
 *   the value between -MOTOR_MAX_VALUE..0..+MOTOR_MAX_VALUE
 *   A value of '0' stops the wheel.

* Initializes the motor driver:
* - IN1 H ; IN2 L --> forward
* - IN1 L ; IN2 H --> backward
* - IN1 =  IN2 	  --> fast stop
* - IN1 != IN2	  --> slow stop
*/
void motorSetPwmBack(int8_t value)
{
	  if (value > MOTOR_MAX_VALUE) value = MOTOR_MAX_VALUE;
	  if (value < -MOTOR_MAX_VALUE) value = -MOTOR_MAX_VALUE;
	  valueBack = value;

	  if (value < 0)
	  {
	    // drive backward
	    value = -value;             // value has to be a positive channel value!
	    MOTOR_BACK_BACKWARDS();
	  }
	  else if (value > 0)
	  {
	    // drive forward
		  MOTOR_BACK_FORWARDS();
	  }
	  else
	  {
	    // stop
		  MOTOR_BACK_STOP();
	  }
	  uint16_t v = (uint16_t)(((FTM0_MOD + 1) * ((uint32_t)value)) / MOTOR_MAX_VALUE);
	  FTM0_C2V = v;
}


/**
 * Command line parser for this file.
 * This code is complete and works.
 *
 * @param[in] cmd
 *   the command to parse
 */

/**
 * Initializes the motor driver:
 * - Motor F PWM: PTA4, FTM0_CH1, Mux:3
 * - Motor F A: PTD5, Mux:1
 * - Motor F B: PTD6, Mux:1
 * - Motor B PWM: PTA5, FTM0_CH2, Mux:3
 * - Motor B A: PTB18, Mux:1
 * - motor B B: PTB19, Mux:1
 */
void motorInit(void)
{
	//configure Motor Pins.
	PORTA_PCR4 = PORT_PCR_MUX(3);
    PORTA_PCR5 = PORT_PCR_MUX(3);
    PORTD_PCR3 = PORT_PCR_MUX(1);
    PORTD_PCR4 = PORT_PCR_MUX(1);
    PORTD_PCR5 = PORT_PCR_MUX(1);
    PORTD_PCR6 = PORT_PCR_MUX(1);

    GPIOD_PDDR = 1<<3 | 1<<4;   //Motor 1            // configure as output
    GPIOD_PDDR = 1<<5 | 1<<6;   //Motor 2            // configure as output

  GPIOD_PSOR = 1<<3 | 1<<4;               // set PTD3 & PTD4 = 1
  GPIOD_PSOR = 1<<5 | 1<<6;               // set PTE5 & PTE6 = 1

  // _todo ML#9.07 configure both channels as edge aligned PWM with low-true pulses
  FTM0_C1SC = FTM_CnSC_MSx(2) | FTM_CnSC_ELSx(2);
  FTM0_C2SC = FTM_CnSC_MSx(2) | FTM_CnSC_ELSx(2);
}
