/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         Common settings of the FTM0
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          26.03.20018
 *
 * $Id: ftm0.h 116 2018-05-11 14:30:59Z zajost $
 *
 *--------------------------------------------------------------------
 */

#ifndef SOURCES_FTM0_H_
#define SOURCES_FTM0_H_

#define FTM0_CLOCK              250000  // 250 kHz
#define FTM0_MODULO             2500//0xffff//10000//0xC350//0x4C4B40//5000000  0x7A120//0x186A0// 100000     :-0xC350  // 50000
void ftm0Init(void);



#endif /* SOURCES_FTM0_H_ */
