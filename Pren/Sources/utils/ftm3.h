/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         Common settings of the FTM3
 * \file
 * \author        Christian Jost, christian.jost@hslu.ch
 * \date          03.04.20018
 *
 * $Id: ftm3.h 116 2018-05-11 14:30:59Z zajost $
 *
 *--------------------------------------------------------------------
 */

#ifndef SOURCES_FTM3_H_
#define SOURCES_FTM3_H_

#define FTM3_CLOCK              250000  // 60 MHz
#define FTM3_MODULO             (1<<14)//0xC350//0x4C4B40//5000000  0x7A120//0x186A0// 100000     :-0xC350  // 50000

void ftm3Init(void);



#endif /* SOURCES_FTM3_H_ */
