/*
 * loadarm.c
 *
 *  Created on: 02.11.2018
 *      Author: zbind
 */
#include"platform.h"
#include"loadarm.h"
#include "Errors.h"
#include"servo.h"
#include "adc.h"
#include "util.h"

general_errors_t armInit(){

	PORTD_PCR0 = PORT_PCR_MUX(1) | PORT_PCR_PE(1) | PORT_PCR_PS(1); //loadsensor
	loadarm.loadsensor = &ReadSens; //TODO Check ReadSens

	FTM0_C6V =0; // Put the servos to 0°
	FTM0_C7V =0;

	loadarm.arm.pin = PORTA_PCR1;
	loadarm.arm.name = 1;
	loadarm.arm.position = 0;

	loadarm.hand.pin = PORTA_PCR2;
	loadarm.hand.name = 2;
	loadarm.arm.position = 0;
	moveServo(&loadarm.hand,0);
	moveServo(&loadarm.arm,0);

	return NO_ERROR;
}

bool ReadSens(void){return (0b1 & ~GPIOD_PDIR);}

general_errors_t moveArmto(angle angle){//move arm forward and backward
	if(loadarm.arm.position > angle){
		for(int x=loadarm.arm.position;x>=angle;){
			moveServo(&loadarm.arm,x);
			utilWaitUs(DELAY);//Wait
			x -= STEP_WIDTH;
		}
	}else{
		for(int x=loadarm.arm.position;x<=angle;){
			moveServo(&loadarm.arm,x);
			utilWaitUs(DELAY);//Wait
			x += STEP_WIDTH;
		}
	}
	return NO_ERROR;
}
general_errors_t Grap(void){//grap the load
	angle j=0;
while(!(0b1 & ~GPIOD_PDIR)){//!loadarm.loadsensor){
	moveServo(&loadarm.hand,(loadarm.hand.position+j));
	utilWaitUs(DELAY);//Wait
	j++;
	if(j>=180)return LOAD_ERROR;
}
utilWaitUs(DELAY);//Wait
moveServo(&loadarm.hand,(loadarm.hand.position+j+5));

return NO_ERROR;
}
general_errors_t Release(void){//release the load
	moveServo(&loadarm.hand,0);
	return NO_ERROR;

}
general_errors_t Load(void){
	general_errors_t error;
	error = moveArmto(LOAD_ANGLE);
	error += Grap();
	error += moveArmto(INITIAL_ANGLE);
	if(error != NO_ERROR){
		//TODO Error handling
	}
	return error;

}
