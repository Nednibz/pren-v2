/*
 * loadarm.h
 *
 *  Created on: 02.11.2018
 *      Author: zbind
 */

#ifndef SOURCES_LOADARM_LOADARM_H_
#define SOURCES_LOADARM_LOADARM_H_
#include "servo.h"
#include "Errors.h"

#define STEP_WIDTH   	1 //TODO genau bestimmenn
#define LOAD_ANGLE 		180
#define INITIAL_ANGLE 	0
#define DELAY			10000 //us to wait
struct loadarm{
	servo arm;
	servo hand;
	bool (*loadsensor)(void);
}loadarm;

general_errors_t armInit(void);

bool ReadSens(void);
general_errors_t moveArmto(angle angle);//move arm forward and backward
general_errors_t Grap(void);//grap the load
general_errors_t Release(void);//grap the load
general_errors_t Load(void); // combined move and grap




#endif /* SOURCES_LOADARM_LOADARM_H_ */
