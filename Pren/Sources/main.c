/**
 *--------------------------------------------------------------------\n
 *          HSLU T&A Hochschule Luzern Technik+Architektur            \n
 *--------------------------------------------------------------------\n
 *
 * \brief         Pren 2019
 * \file
 * \author        Daniel <zbinden, daniel.zbinden@hslu.ch
 * \date          12.03.2018
 *
 * $Id: main.c zbinden $
 *
 *--------------------------------------------------------------------
 *--------------------------------------------------------------------
*/
#include 	<stdio.h>
#include 	<stdlib.h>
//#include 	"stdbool.h"
#include 	"platform.h"
#include 	"ftm0.h"
#include 	"ftm3.h"
#include 	"util.h"

#include 	"uart.h"
#include 	"term.h"
#include 	"i2c.h"
#include 	"motor.h"
//#include 	"quad.h" // TODO implement Quadraturencoder
#include 	"drive.h"
#include 	"led.h"
#include 	"lichtschranke.h"
#include 	"servo.h"
#include 	"ultrasonic.h"
#include 	"acceleration.h"
#include 	"loadarm.h"

//Chack UART commands
#define CheckCommand(buf, command)   (strncmp(buf, command, sizeof(command)-1) == 0)

// calulate nr of TOF count for a given number of milliseconds
//#define TOFS_MS(x)   ((uint16_t)(((FTM3_CLOCK / 1000) * x) / (FTM3_MODULO + 1)))

int16_t speed;
enum{init,load,drive,stopp,wait,test}sequence;

/*
void ProcessDrive(void)
{
  static uint16_t i;
  if (i++ == TOFS_MS(25)) {// run pid worker every 25ms
    i=0;
    if (pwrSwitchEnabled()) {

      speedR = speedL = 0;
      if (speed != 0)
      {
        int16_t dir = lsGetDir() * 11;
        uint16_t offset;
        if (dir > 0) offset = dir + (speed / 25);
        if (dir < 0) offset = dir - (speed / 25);
        speedL = speed - offset;
        speedR = speed + offset;
        if (dir == 127) speed = speedL = speedR = 0;
      }

      driveSetSpeed(speedL, speedR);
      driveToWork();
    }
    else {
      driveToWork();
    }
  }
}


*/

void getInfo(){ //Collect Sensor data and write it to both Uart ports
	acceleration ACC =  getacceleration();
	uint32_t US = getDistance();
	char answer[5];

	termWrite("info:");
	utilNum16sToStr(&answer,5,speed);
	termWrite(answer);
	termWrite("/");
	utilNum16sToStr(&answer,5,ACC.x);
	termWrite(answer);
	termWrite(",");
	utilNum16sToStr(&answer,5,ACC.y);
	termWrite(answer);
	termWrite(",");
	utilNum16sToStr(&answer,5,ACC.z);
	termWrite(answer);
	termWrite(",");
	utilNum16sToStr(&answer,5,ACC.validity);
	termWrite(answer);
	termWrite("/");
	utilNum16sToStr(&answer,5,US);
	termWrite(answer);
	termWrite("\n");
}

/**
 *
 * @param cmd
 * @return
 * List of commands:
 * 		- info: 						gathers sensor data and outputs it (see getInfo())
 * 		- startLoad, startDrive, stop:	acknowledge to go into next sequence
 * 		- IR:							Input if the signal was successfully recognized
 * 		- servoARm, servoHand:			write angle of the servos
 * 		- extendArm:					extend arm slowly to the input angle
 * 		- armGrap, armRelease:			grap or release the cube
 * 		- Load:							combination of extendArm and armGrap
 * 		- driveStep:					drive (input) milimeters +forward -backward
 * 		- driveSetSpeed:				set the Power for the motors [-100% - 100%]
 * 		- i2cTest:						Test the acceleration Sensor for response
 */

general_errors_t ParseCommand(char *cmd){ //
	general_errors_t result = COMMAND_PARS_ERROR;
	int param = 0;
	result = NO_ERROR;
	 if (strncmp(cmd, "info", sizeof("info")-1) == 0)
		  	  {
		  		getInfo();
		  	    result = NO_ERROR;
		  	  }

	 else if (strncmp(cmd, "startLoad", sizeof("startLoad")-1) == 0)
	  {
		  if(sequence == init){
			  sequence = load;
		  	  result = NO_ERROR;
		  }else{
			  result =VEHICLE_EXPLOSION_ERROR;
		  }
	  }
	  else if (strncmp(cmd, "startDrive", sizeof("startDrive")-1) == 0)
	  {
		  if(sequence == load){
			  sequence = drive;
		  	  result = NO_ERROR;
		  }else{
			  result =VEHICLE_EXPLOSION_ERROR;
		  }
	  }

	  else if (strncmp(cmd, "stop", sizeof("stop")-1) == 0)
	  	  {
		  if(sequence == drive){
			  sequence = stopp;
		  	  result = NO_ERROR;
		  }else{
			  result =VEHICLE_EXPLOSION_ERROR;
		  }
	  	  }
	  else if (strncmp(cmd, "IR", sizeof("IR")-1) == 0)
	  	  {
	  		  cmd += sizeof("IR");
	  		  utilScanDecimal16s(&cmd, &param);
	  		  uint16_t stopDistance = param;
	  		  if(IR){sequence = stopp;} //TODO implement image recognizion behaviour
	  	    result = NO_ERROR;
	  	  }
	  else if (strncmp(cmd, "servoArm", sizeof("servoArm")-1) == 0)
	  {
		  cmd += sizeof("servoArm");
		  utilScanDecimal16s(&cmd, &param);
		  moveServo(&loadarm.arm,(angle)param);
	    result = NO_ERROR;
	  }
	  else if (strncmp(cmd, "servoHand", sizeof("servoHand")-1) == 0)
	  {
		  cmd += sizeof("servoHand");
		  utilScanDecimal16s(&cmd, &param);
		  moveServo(&loadarm.hand,(angle)param);
	    result = NO_ERROR;
	  }
	  else if (strncmp(cmd, "armExtend", sizeof("armExtend")-1) == 0)
	  {
		  cmd += sizeof("armExtend");
		  utilScanDecimal16s(&cmd, &param);
		  moveArmto((angle)param);
	    result = NO_ERROR;
	  }
	  else if (CheckCommand(cmd, "armGrap"))
	 	  {
	 	    result = Grap();
	 	  }
	 else if (CheckCommand(cmd, "armRelease"))
	 	  {

	 	    result = Release();
	 	  }
	 else if (CheckCommand(cmd, "Load"))
	 	 	 	  {
	 	 	 result = Load();
	 	 	 	  }
	  else if (strncmp(cmd, "driveStep", sizeof("driveStep")-1) == 0)
	  {
		  cmd += sizeof("driveStep");
		  utilScanDecimal16s(&cmd, &param);
		  //driveStep(param);
	    result = NO_ERROR;
	  }
	  else if (strncmp(cmd, "driveSetFront", sizeof("driveSetFront")-1) == 0) //TODO combine front and back to one function
	  {
		  cmd += sizeof("driveSetFront");
		  utilScanDecimal16s(&cmd, &param);
		  //FTM0_C1V = param;
		  motorSetPwmFront(param);
		  //driveSetSpeed(param);
	    result = NO_ERROR;
	  }
	  else if (strncmp(cmd, "driveSetBack", sizeof("driveSetBack")-1) == 0)
	  {
		  cmd += sizeof("driveSetBack");
		  utilScanDecimal16s(&cmd, &param);
		  motorSetPwmBack(param);
		  //driveSetSpeed(param);
	    result = NO_ERROR;
	  }
	  else if (strncmp(cmd, "i2ctest", sizeof("i2ctest")-1) == 0)
	  	  {
		  char i = (char)i2cTest(ACC_ADDRESS);
  		  termWrite(i);
	  	    result = NO_ERROR;
	  	  }
	  else{
	  		  termWrite(cmd);
	  	  }
	  return result;
}
/**
 * The main function of the Pren app.
*/
void main(void)
{
		/** Ablauf:
		 * pwrButton 		-> Tiny initialisierung
		 * tiny: Error 		-> Pi
		 * Tiny: Load
		 * Tiny Sequence	-> Pi
		 * Pi: Sequence		-> Tiny
		 * Tiny: Drive
		 * Tiny: Rounds		-> Pi
		 * Tiny: OBJDETECT	-> Pi
		 * Pi: Bildverarbeitung
		 * Pi: OBJDETECT	-> Tiny
		 * Tiny: Rounds		-> Pi
		 * Pi: Stop		-> Tiny
		 * Tiny: Stop
		 * */


 //**************************  variables  **************************//
  static general_errors_t errors = NO_ERROR;
  uint8_t Roundcounter = 0;
  int16_t j = 0; //TODO delete
  uint16_t stopDistance = 0;

 //**************************  initialisation  **************************//
  sequence = init;
  if(sequence == init){
  ftm0Init();
  ftm3Init();
  uart0Init(57600); //used for communication with PI //TODO look if needed maybe for pi communication
  termInit(57600);	//used for debug
  i2cInit();		// used for acceleration sensor

  errors += UltraSonicInit();
  errors += ServoInit();
  errors += LSInit();//light barriers
  errors += MSInit();//microswitches aka Endschalter
  errors += armInit();
  errors += AccInit();//acceleration
  motorInit();
  //errors += quadInit();
  //errors += driveInit();
  //errors += ledInit();

  if(errors == NO_ERROR){
	sequence = wait;
  }else{

	  utilWaitUs(5000);
  }
  sequence = test; //TODO delete
  }
  //**************************  load sequence  **************************//

while(sequence == load){
   int distance=0;
   int step = STEP_WIDTH;
   driveSetSpeed(LOAD_SPEED);
   while(!LS1()){// drive to the first sensor
   if(x>MAX_LOAD_DISTANCE){	// to far ? go back TODO define MAX_LOAD_DISTANCE
	   driveSetSpeed(-LOAD_SPEED);
   }}
   if(LS1() ){// get to the edge of the cube
	   driveSetSpeed(0);
	   errors += Load();
	   utilWaitUs(5000);
   }
//   while(!LS2()){//drive to the second sensor
//   //driveStep(step);
//   distance += step;
//   }
//   //driveStep(-distance/2); // drive half way back to align with the arm TODO determine if its really half way
//

   if(errors == NO_ERROR){//==> transmit Data to PI
	   uart0WriteLine("startLoad");
	  }
   else{
		  char answer[4];
		  utilNum16sToStr(&answer,5,(uint16_t)errors);
		  uart0Write("error: ");
		  uart0Write(answer);
		  uart0Write(NEW_LINE);
   }
   sequence = wait;
}
  //**************************  drive sequence  **************************//

	if(sequence == drive){ //Accelerate to max Speed
 for(int j=0;j<=MAX_SPEED;j+=1){  // accelerate TODO define PWM step width and implement acceleration function. also needed below
 driveSetSpeed(j);
 utilWaitUs(5000);//TODO define timeout
 }}

 while(sequence == drive){
 acceleration acc = getacceleration();
 if(Roundcounter >= 1){
	 driveSetSpeed(IR_SPEED);
 }
 else if(acc.x >= MAX_SAFE_ACC){  //TODO define Axis x? y? z?
 driveSetSpeed(j-5);
 utilWaitUs(1000);
 }else if(j< MOTOR_MAX_VALUE)
 {	   driveSetSpeed(j++);}

 //==> transmit Data to PI
 //==>Sensor detected something{Uart_message(go);}//Interupt style uart message
 //==> Error image reconition {drive slower}/ after accelerate again maybe?
 //==> Roundcounter {if(==2){sequence = stopp;}} Roundcounter with interrupt
if(Roundcounter >= 2){
	  sequence = stopp;
}
	      if(errors == NO_ERROR){
	   uart0WriteLine("startDrive");
	   sequence = wait;
	  }else{//==> Error handling;

	 }
 }

  //**************************  stop sequnece  **************************/

  while(sequence == stopp){
	  driveSetSpeed(LOAD_SPEED);
	  uint16_t time = stopDistance/LOAD_SPEED;//TODO stop sequence
	  time  -= 1;
	  utilWaitUs(1000*1000*time)
	  // Drive to signal TODO behaviour is depending on PI for the stopp sequence implement as a function called from UART commands
	  // Stop(uint16 StopIn_mm){}
  }

  //**************************  wait sequnece  **************************/

  while(sequence == wait){
	  termDoWork();
	  utilWaitUs(5000);

  }

   //************************************** Sequence for testing*****************************
   while(sequence == test){

	   termDoWork();
	   utilWaitUs(50000);
	    acceleration acc = getacceleration();
	    if(acc.x >= MAX_SAFE_ACC){  //TODO define Axis x? y? z?
	    	j=j-5;
	    	if(j<0){j=0;}
	    motorSetPwmFront(j);
	    }else{
	    	motorSetPwmFront(j++);
	    	if(j>30){j=30;}
	    }

   }
}

