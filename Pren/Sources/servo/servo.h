/*
 * servo.h
 *
 *  Created on: 02.11.2018
 *      Author: zbind
 */

#ifndef SOURCES_SERVO_SERVO_H_
#define SOURCES_SERVO_SERVO_H_

#include"Errors.h"
#define SERVO_MOD              5000  // 250 kHz -> 20MS  duty cycle
#define SERVO_MAX_PWM              500  // 1MS
#define SERVO_MIN_PWM              100  // 2MS

typedef uint8_t angle;
typedef struct servo {
	   uint32_t position;
	   uint32_t pin;
	   uint8_t name;
	}servo;
general_errors_t ServoInit();
general_errors_t moveServo_old(servo ser,angle ang);//servo,angle);
general_errors_t moveServo(servo *ser,angle ang);//servo,angle);


#endif /* SOURCES_SERVO_SERVO_H_ */
