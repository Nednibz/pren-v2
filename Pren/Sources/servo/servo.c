/*
 * servo.c
 *
 *  Created on: 02.11.2018
 *      Author: zbind
 */

//todo Pren# Servo write and maybe read
#include"platform.h"
#include"servo.h"
#include"ftm0.h"
#include"Errors.h"
general_errors_t ServoInit(){//Todo Pren# implement error cases

	FTM0_C6SC = FTM_CnSC_MSx(2) | FTM_CnSC_ELSx(2);	// Set the MSA and MSB bit to 10 for PWM  for Servo arm
	FTM0_C7SC = FTM_CnSC_MSx(2) | FTM_CnSC_ELSx(2);	// Set the MSA and MSB bit to 10 for PWM  for Servo hand

	GPIOA_PDDR |= (1<<1) | (1<<2);
	GPIOA_PSOR = 1<<1 | 1<<2;
	GPIOA_PCOR = 1<<1 | 1<<2;
	GPIOA_PSOR = 1<<1 | 1<<2;
	PORTA_PCR1 = PORT_PCR_MUX(3); // PTA1 Servo1
	PORTA_PCR2 = PORT_PCR_MUX(3); // PTA2 Servo2

	return NO_ERROR;
}
general_errors_t moveServo(servo *ser,angle ang){
	if(ang >=0 & ang <=180){
	ser->position = ang;
	int angle = ang * SERVO_MAX_PWM/180 + SERVO_MIN_PWM; // map the angle between the max and min PWM output
	if (ser->name == 1){FTM0_C6V = angle;}else if(ser->name == 2){FTM0_C7V = angle;}else{return SERVO_ERROR;}
	return NO_ERROR;
	}else{
		return SERVO_ANGLE_FORMAT_ERROR;
	}
}

