/*
 * ultrasonic.c
 *
 *  Created on: 09.11.2018
 *      Author: zbind
 */
#include "platform.h"
#include "ftm3.h"
#include "ultrasonic.h"
#include "Errors.h"
#include "term.h"


	//TODO Pren# implement error cases & return value
general_errors_t UltraSonicInit(){
	  PORTC_PCR8 = PORT_PCR_MUX(3);
	  PORTC_PCR9 = PORT_PCR_MUX(3);
	  FTM3_C5V =0;
	  return NO_ERROR;
}
uint32_t getDistance(void){
	return (uint32_t)meassure;
}

void FTM3CH5_IRQHandler(){
	static int valOld;
	static int valNew;
	static bool edge;
	uint32_t ticks;
	valNew = FTM3_C5V;

	if(valOld == 0){
		edge = 1;
	}
	if(edge == 0){
		ticks = valNew *4000;
		double time = (double)(ticks)/FTM3_CLOCK; //t in miliseconds
		meassure = time * SPEED_OF_SOUND;
		meassure /=4;
		meassure -= 76;
		meassure = meassure *1.018 -1.18; // 1.024
		//termWriteNum32s((uint32_t)meassure); // in milimeters
		//termWriteChar('\n');
	}
	if(meassure <= IR_DISTANCE){
		char answer[3];
		termWrite("IR:");
		//utilNum32sToStr(&answer,3,(uint32_t)meassure);
		//termWrite(answer);
		termWriteNum32s(meassure);
		termWrite("\n");
	}
	valOld = valNew;
	edge = !edge;
	FTM3_C5SC &= ~FTM_CnSC_CHF_MASK;
	FTM3_C5V = 0;

}
