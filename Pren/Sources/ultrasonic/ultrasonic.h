/*
 * ultrasonic.h
 *
 *  Created on: 02.11.2018
 *      Author: zbind
 */

#ifndef SOURCES_ULTRASONIC_ULTRASONIC_H_
#define SOURCES_ULTRASONIC_ULTRASONIC_H_
#include"Errors.h"

//todo Pren# Ultrasonic
#define ECHOPIN				0x0;	// Inputpin for the Ultrasonic sensor
#define USPIN 				0x0;       // Outputpin for the Ultrasonic sensor
#define SAMPLETIME			0x0;  //sapletime in us
#define SPEED_OF_SOUND 		343.2; //mm/ms
#define ULTRASONIC_IMPULSE 	30; //PWM for meassurement
#define IR_DISTANCE			150


static int ChannelCalue;
double meassure;

general_errors_t UltraSonicInit(void);
uint32_t getDistance(void);
#endif /* SOURCES_ULTRASONIC_ULTRASONIC_H_ */
