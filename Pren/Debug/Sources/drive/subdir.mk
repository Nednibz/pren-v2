################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/drive/drive.c \
../Sources/drive/linesens.c \
../Sources/drive/motor.c \
../Sources/drive/quad.c 

OBJS += \
./Sources/drive/drive.o \
./Sources/drive/linesens.o \
./Sources/drive/motor.o \
./Sources/drive/quad.o 

C_DEPS += \
./Sources/drive/drive.d \
./Sources/drive/linesens.d \
./Sources/drive/motor.d \
./Sources/drive/quad.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/drive/%.o: ../Sources/drive/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"../Sources" -I"../Sources/adc" -I"../Sources/com" -I"../Sources/drive" -I"../Sources/Errors" -I"../Sources/io" -I"../Sources/i2c" -I"../Sources/sound" -I"../Sources/utils" -I"../Sources/servo" -I"../Sources/acceleration" -I"../Sources/ultrasonic" -I"../Sources/loadarm" -I"../Includes" -std=c99 -Wa,-adhlns="$@.lst" -v -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


