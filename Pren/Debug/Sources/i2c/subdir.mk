################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/i2c/SSD1306.c \
../Sources/i2c/colSens.c \
../Sources/i2c/eeprom.c \
../Sources/i2c/i2c.c 

OBJS += \
./Sources/i2c/SSD1306.o \
./Sources/i2c/colSens.o \
./Sources/i2c/eeprom.o \
./Sources/i2c/i2c.o 

C_DEPS += \
./Sources/i2c/SSD1306.d \
./Sources/i2c/colSens.d \
./Sources/i2c/eeprom.d \
./Sources/i2c/i2c.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/i2c/%.o: ../Sources/i2c/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"../Sources" -I"../Sources/adc" -I"../Sources/com" -I"../Sources/drive" -I"../Sources/Errors" -I"../Sources/io" -I"../Sources/i2c" -I"../Sources/sound" -I"../Sources/utils" -I"../Sources/servo" -I"../Sources/acceleration" -I"../Sources/ultrasonic" -I"../Sources/loadarm" -I"../Includes" -std=c99 -Wa,-adhlns="$@.lst" -v -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


